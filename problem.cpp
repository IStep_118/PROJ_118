#include <iostream>

#include <cmath>

#include "problem.h"



using namespace std;



void CProblem::SetValues()

{

cout << "Number: ";

cin >> Numeral;

cout << "Arithmetic operation (plus,minus,multip,divis,pow,sqrt,sin,cos,tan): ";

cin >> Operation;

}



bool CProblem::Calculate()

{

if(Operation == "plus")

{

cout << "Second summand: ";

cin >> SecondNumeral;

Result = Numeral + SecondNumeral;

return true;

}

else if(Operation == "minus")

{

cout << "Second summand: ";

cin >> SecondNumeral;

Result = Numeral - SecondNumeral;

return true;

}

else if(Operation == "multip")

{

cout << "Multiplier: ";

cin >> SecondNumeral;

Result = Numeral * SecondNumeral;

return true;

}

else if(Operation == "divis")

{

cout << "Divider: ";

cin >> SecondNumeral;

if(SecondNumeral == 0)

{

Error = "Error: division by zero";

return false;

}

else

{

Result = Numeral/SecondNumeral;

return true;

}

}

else if(Operation == "pow")

{

cout << "Degree: ";

cin >> SecondNumeral;

Result = pow(Numeral,SecondNumeral);

return true;

}

else if(Operation == "sqrt")

{

Result = sqrt(Numeral);

return true;

}

else if(Operation == "sin")

{

Result = sin(Numeral);

return true;

}

else if(Operation == "cos")

{

Result = cos(Numeral);

return true;

}

else if(Operation == "tan")

{

Result = tan(Numeral);

return true;

}

else

{

Error = "Action input error.";

return false;

}

}



void CProblem::Solve()

{

if(Calculate() == true)

cout << Result << "\n";

else

cout << Error << "\n";

}
