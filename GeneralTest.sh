#!/bin/bash
pwd=$PWD/Tests
Path=/tmp/kalkul
mkdir /tmp/kalkul >/dev/null 2>&1 || true
PIDG=$$
mkdir $Path/$PIDG
pidarray=()
namearray=($(ls $pwd))
testarray=()
resarray=()

for i in ${!namearray[@]}; do
TestFile=$pwd/${namearray[$i]}
if [ -x "$TestFile" ]; then
($TestFile $PIDG $Path >/dev/null 2>&1 ) &
PID=$!
pidarray+=($PID)
testarray+=(${namearray[$i]})
fi
done

GRES=0

for i in ${!pidarray[@]}; do
wait ${pidarray[$i]}
RES=$?
resarray+=($RES)
echo "${testarray[$i]} (PID ${pidarray[$i]}) done with $RES"
GRES=$(( $GRES + $RES ))
done

if [[ $GRES == 0 ]]; then
  echo "Tests succeed"
  exit 0
fi

echo "Tests failed"
exit 1

