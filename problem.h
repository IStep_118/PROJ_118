#ifndef PROBLEM_H_

#define PROBLEM_H_



#include <string>



using namespace std;



class CProblem

{

private:

float Numeral;

float SecondNumeral;

string Operation;

float Result;

string Error;

bool Calculate();

public:

void SetValues();

void Solve();

};



#endif /*PROBLEM_H_*/
