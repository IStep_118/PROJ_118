#!/bin/bash

{
  set -e
  sleep 5
  kill -USR1 $$ >/dev/null 2>&1
} &
trap 'echo "Test failed 3" && exit 1' USR1
empty -f -i /tmp/inp$$ -o /tmp/oup$$ ./kalkul
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Number:" "2\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "tan):" "\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Degree:" "10\n"
empty -s -o /tmp/inp$$ "\n"

echo "Test success 3"
echo "$1"
echo "$2"
