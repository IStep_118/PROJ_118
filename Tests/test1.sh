#!/bin/bash
{
  set -e
  sleep 5
  kill -USR1 $$ >/dev/null 2>&1
} &
trap 'echo "Test failed 1" && exit 1' USR1
empty -f -i /tmp/inp$$ -o /tmp/oup$$ ./kalkul
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Number:" "2\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "tan):" "multip\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Multiplier:" "4\n"
empty -s -o /tmp/inp$$ "\n"

echo "Test success 1"

