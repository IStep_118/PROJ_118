#!/bin/bash
{
  set -e
  sleep 5
  kill -USR1 $$ >/dev/null 2>&1
} &
trap 'echo "Test failed 2" && exit 1' USR1
empty -f -i /tmp/inp$$ -o /tmp/oup$$ -L test.log ./kalkul
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Number:" "5\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "tan):" "plus\n"
empty -w -i /tmp/oup$$ -o /tmp/inp$$ "Second summand:" "4\n"
empty -s -o /tmp/inp$$ "\n"
echo "Test success 2"

