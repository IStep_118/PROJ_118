kalkul: problem.o main.o
	g++ problem.o main.o -o kalkul
problem.o: problem.cpp problem.h
	g++ -c problem.cpp
main.o: main.cpp problem.h
	g++ -c main.cpp
clean:
	rm -f kalkul problem.o main.o
install:
	cp kalkul /usr/local/bin/kalkul
uninstall:
	rm -f /usr/local/bin/kalkul
